---
layout: handbook-page-toc
title: Solutions Architects Content Calendar
description: "Content calendar for Solutions Architects."
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

The purpose of this effort is to consistently create content that:

- Helps customers be successful in an [async](https://about.gitlab.com/company/culture/all-remote/asynchronous/) manner
- Supports our GTM efforts by scaling the solutions architecture knowledge and reach
- Builds thought leadership across the solutions architecture community
- Enables solutions architects to spend more time having meaningful conversations with customers

This is not meant to limit creativity. It's a [boring](/handbook/values/#boring-solutions) solution to systematically and consistently create content beneficial to our customers. 

## Content process

1. Sign up as an author by adding your username to the calendar table next to the publish date of your choice. Multiple people are welcomed and encouraged to post during the same week.
2. Choose a topic from the [demo wishlist](https://drive.google.com/drive/u/0/search?q=title:%225%20Minute%20Demo%20Framework:%20Demo%20Wishlist%22%20type:document)(_internal only_) or [framework tutorials](https://drive.google.com/drive/u/0/search?q=title:%22Framework%20Tutorials%22%20type:sheet)(_internal only_) list. Add the topic to the calendar table next to your username and publish date. Feel free to bring your own topic!
3. Create your video or blog post. Please store all code examples in the [Guided Explorations](https://gitlab.com/guided-explorations) group to share with customers.
4. If creating a video, please add your video to the [Solutions Architecture GitLab Unfiltered Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Ko87g05LlHroe7eLPzCPJUY).

## Calendar

| Publish Week | Author                                            | Topic                                                    | Content Type |
|--------------|---------------------------------------------------|----------------------------------------------------------|--------------|
| 2023-02-06   | [Jeremy Wagner](https://gitlab.com/jeremywagner)  | How to deploy a React application to AWS S3 using GitLab |              |
| 2023-02-20   | [Julie Byrne](https://gitlab.com/juliebyrne)      | Getting Started with GitLab Security                     | Blog         |
| 2023-02-27   | [Noah Ing](https://gitlab.com/noah.ing)           | Terraform Multi Environments with GitLab                 |              |
| 2023-03-06   | [Siddharth Mathur](https://gitlab.com/smathur-gl) |                                                          |              |
| 2023-03-13   | [Sophia Manicor](https://gitlab.com/smanicor)     |                                                          |              |
| 2023-03-20   | [Sam Morris](https://gitlab.com/sam)              | CI and Secure in Mono-Repos                              |              |
| 2023-03-27   | [Michael Balzer](https://gitlab.com/mbalzer)      |                                                          |              |
| 2023-04-03   | [Cailey Pawlowski](https://gitlab.com/cpawlowski) | Analytics for Premium GitLab Users                       |              |
| 2023-04-10   |                                                   |                                                          |              |
| 2023-04-17   | [Julie Byrne](https://gitlab.com/juliebyrne)             |  Preparing for a GitLab data migration  | Blog, related [video](https://youtu.be/L11mZqQKuwo) already created |
| 2023-04-24   |                                                   |                                                          |              |

## Published Blogs & Videos

| Publish Date | Author                                                                                       | Topic                                                                                                                                            | Content Type |
|--------------|----------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------|--------------|
| 2022-08-02   | [Madou Coulibaly](https://gitlab.com/madou) and [Joe Randazzo](https://gitlab.com/jrandazzo) | [ How to provision dynamic review environments using merge requests and Argo CD ]( /blog/2022/08/02/how-to-provision-reviewops/ )                | Blog         |
| 2022-09-23   | [Joe Randazzo](https://gitlab.com/jrandazzo)                                                 | [ CI/CD Modernization with GitLab ]( https://www.youtube.com/watch?v=QGAaif8lY0A&ab_channel=GitLabUnfiltered )                                   | Video        |
| 2022-10-06   | [Jeremy Wagner]( https://gitlab.com/jeremywagner )                                           | [ How to automate testing for a React application with GitLab ]( /blog/2022/11/01/how-to-automate-testing-for-a-react-application-with-gitlab/ ) | Blog         |
| 2022-01-27   | [ Sarah Bailey ]( https://gitlab.com/sbailey1 )                                              | [ All About The Merge Request ]( https://www.youtube.com/watch?v=eTIVgyknA9w )                                                                   | Video        |
|   2023-01-09  | [Julie Byrne](https://gitlab.com/juliebyrne) | [Migrating from Self-Managed to SaaS](https://youtu.be/L11mZqQKuwo)     | Video |
|   2023-02-06  | [Siddharth Mathur](https://gitlab.com/smathur) | [Setting Up OIDC to Get Credentials from Google Cloud](https://www.youtube.com/watch?v=Psfy3dIa6w8&list=PL05JrBw4t0KrjbWGBOq710-WAUpQTVial)     | Video |

## DRI rotation

Every quarter, the DRI for maintaining the calendar will rotate. The main responsiblity of the DRI is finding volunteers to fill the calendar to ensure consistent content creation and to identify the next DRI.

FY24 Q1 DRI: [Jeremy Wagner](https://gitlab.com/jeremywagner)

FY24 Q2 DRI: Open to volunteers

## How to contribute

[Everyone can contribute](https://about.gitlab.com/company/mission/#background). This effort was started to combine multiple initiatives to get content to help customers. Have an idea? Submit an MR and improve the process.

Some easy ways to contribute to this effort are:

1. Volunteer for a spot on the calendar
2. Volunteer to be the DRI for a quarter
3. Adding requested demos to the 5 minute demo wishlist
4. Add frameworks to the frameworks tutorial list
5. Expand the deployment options for various cloud providers
6. Identify the best way to share content with the GTM team so they are aware to share with customers
