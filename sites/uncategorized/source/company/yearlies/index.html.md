---
layout: handbook-page-toc
title: "Yearlies"
description: ""
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Yearlies

Yearlies are the annual goals for the company. Yearlies should have measurable deliverables.

## Alignment 

Yearlies connect our [3 year strategy](/company/strategy/) to our shorter-term quarterly objectives (OKRs). Achieving our yearlies creates progress towards achieving our [strategy](/company/strategy/), and as a result, moves us closer to achieving our [vision](/company/vision/).

### Three Year Strategy 

1. Yearlies are informed by the [three-year strategy](/company/strategy/). 
1. Each yearly is aligned to one of the three pillars of the [three year strategy](/company/strategy/#three-year-strategy). 
1. There should be at least one yearly for each strategic pillar from [three year strategy](/company/strategy/#three-year-strategy).

### Annual Plan and Yearlies

1. Yearlies come before the [Annual Plan](/handbook/finance/financial-planning-and-analysis/#plan). 
1. Yearlies contain our priorities for the fiscal year while the Annual Plan contains our budgets and our financials.  
1. We first determine our priorities for the upcoming year in the form of Yearlies, then we use these priorities to inform the budget in the Annual Plan process. 

The Annual Plan process [finishes two quarters after](https://about.gitlab.com/company/offsite/#offsite-topic-calendar) Yearlies are finalized. As a result, Yearlies may become outdated since being established. In line with our value of [Iteration](https://about.gitlab.com/handbook/values/#iteration), Yearlies are [reviewed each E-Group offsite](https://about.gitlab.com/company/offsite/#recurring-discussion-topics) and updated as needed. Annual Plan and Yearlies should be synced when Annual Plan is finalized.

### Objectives and Key Results (OKRs)

1. [Objectives and Key Results (OKRs)](/company/okrs/) are our quarterly priorities that create progress for our yearly goals.
1. As a result, [OKRs](/company/okrs/)) are aligned to one of the yearlies. 
1. While [OKRs](/company/okrs/) are not directly aligned to one of the three pillars of the [three year strategy](/strategy/#three-year-strategy), since OKRs are aligned to one of the yearlies, and the yearlies are aligned to one of the pillars, OKRs are indirectly aligned to one of the strategic pillars.
from our [cadence](/company/cadence/).
1. [OKRs](/company/okrs/) have a duration of one quarter while Yearlies are annual goals with a duration of a year.
1. [OKRs](/company/okrs/) are composed of Objectives and Key Results while Yearlies have only one component, the annual goal.

## Cadence

1. The [three year strategy](/company/strategy/#three-year-strategy) is on a 3 year cadence and is inspiration for the Yearlies, which are on a 1 year [cadence](/company/cadence/#year). The three year strategy is reviewed as part of [E-Group offsite calendar](/company/offsite/#offsite-topic-calendar). 
1. Yearlies are established during E-Group offsite as a once-a-year topic in the [offsite topic calendar](https://about.gitlab.com/company/offsite/#offsite-topic-calendar). 
1. Yearlies are the company's goals for the next 12 months unless the Yearly is achieved sooner than 12 months or E-Group decides that a Yearly should be updated. 
1. In line with our value of [Iteration](https://about.gitlab.com/handbook/values/#iteration), Yearlies are reviewed during each E-Group offsite as a [recurring discussion topic](/company/offsite/#recurring-discussion-topics) and updated or replaced if the Yearly needs to be changed sooner than 12 months after being established. 

## FY24 Yearlies

FY24 Yearlies and additional detail can be found by [searching for the FY24 Yearlies Google Doc](https://drive.google.com/drive/search?q=%22FY24%20Yearlies%22).

### Strategic Pillar 1: [Customer Results](/company/strategy/#1-customer-results) 

In order to make progress on our strategic pillar of [Customer Results](/company/strategy/#1-customer-results), we will:

1. Increase Ultimate adoption to increase Ultimate up-tiers by $X
1. Increase Sales Efficiency to grow operating income (NGOI) by X% 
1. Increase User Awareness as evidenced by Y% uplift in aided/unaided awareness

### Strategic Pillar 2: [Mature the Platform](/company/strategy/#2-mature-the-platform)

In order to make progress on our strategic pillar of [Maturing the Platform](#2-mature-the-platform), we will:

1. Close the gap with the best in class (BIC) competitors for each of our stages as evidenced by meeting or exceeding [BIC competitor capabilities](https://about.gitlab.com/competition/) for every stage
1. Launch [GitLab Dedicated](https://about.gitlab.com/direction/saas-platforms/dedicated/) full GA
1. Scale GitLab.com via disaster recovery and [Pods](/direction/pods/)

### Strategic Pillar 3: [Grow Careers](/company/strategy/#3-grow-careers) 

In order to make progress on our strategic pillar of [Grow Careers](/company/strategy/#3-grow-careers), we will:

1. Maintain GitLab's fast pace
1. Increase Team Diversity as evidenced by increasing URG from X% to Y%
1. Enable People Managers to deliver results as evidenced by 95% of people managers completing [Elevate](/handbook/people-group/learning-and-development/elevate/)
