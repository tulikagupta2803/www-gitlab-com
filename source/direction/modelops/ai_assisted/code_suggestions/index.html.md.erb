---
layout: sec_direction
title: "Category Direction - Code Suggestions"
description: "Code recommendations and suggestions within the GitLab WebIDE and VS Code Extension"
canonical_path: "/direction/modelops/ai_assisted/code_suggestions"
noindex: true
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Code Suggestions

| | |
| --- | --- |
| Stage | [ai_assisted](/direction/ai_assisted/) |
| Maturity | [minimal](/direction/maturity/) |
| Content Last Reviewed | `2023-02-01` |


### Introduction and how you can help

Thanks for visiting this category direction page on Code suggestions in GitLab. This page belongs to the [AI Assisted](/..) group of the [ModelOps stage](../modelops) and is maintained by Neha Khalwadekar (nkhalwadekar at gitlab dot com).

This direction page is a work in progress, and [everyone can contribute](https://about.gitlab.com/company/mission/#contribute-to-gitlab-application). We welcome feedback, [bug reports](https://gitlab.com/gitlab-org/gitlab/-/issues/new?&issuable_template=Bug), [feature proposals](https://gitlab.com/gitlab-org/gitlab/-/issues/new?issuable_template=Feature%20proposal%20-%20detailed), and [community contributions](https://about.gitlab.com/community/contribute/)..

 - Please comment and contribute in the linked [issues](#TODO) and [epics](#TODO) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
 - Please share feedback directly via email or on a video call. If you&apos;re a GitLab user and have direct knowledge of your need for code suggestions, we&apos;d especially love to hear from you.
- Consider signing up for [First Look](https://about.gitlab.com/community/gitlab-first-look/).

## Overview

We intend to make code suggestions available for any external IDE and terminal for ultimate users, starting with GitLab's VS Code plugin and new Web IDE. GitLab aspires to build AI based solutions to increase developer productivity by helping them code faster and more efficiently. Get code suggestions that match a repository context and style conventions, and cycle through different options to decide what to accept, reject, or edit.


### Target Audience
Initially we are focused on traditional developer personas including:
1. [Sasha (Software Developer)](https://about.gitlab.com/handbook/product/personas/#sasha-software-developer)
1. [Priyanka (Platform Engineer)](https://about.gitlab.com/handbook/product/personas/#priyanka-platform-engineer)
1. [Simone (Software Engineer in Test)](https://about.gitlab.com/handbook/product/personas/#simone-software-engineer-in-test)
1. [Delaney (Development Team Lead)](https://about.gitlab.com/handbook/product/personas/#delaney-development-team-lead)

In the future we may expand to security personas to help write more secure code and review code for security vulnerabilities and fix them early in the software development lifecycle (SDLC), before you commit.

### Challenges to Address

By implementing AI Assisted Code Suggestions in integrated development environments (IDEs), we believe that the software development lifecycle will be transformed in the following ways:

1. **Increased productivity**: Developers should be able to write code faster and more efficiently, with the AI suggesting appropriate code snippets and improvements in real-time.
1. **Reduced errors**: The AI should be able to detect and suggest corrections for errors as they occur, reducing the time and effort spent on debugging.
1. **Improved code quality**: The AI should be able to suggest refactoring and best practices, leading to more maintainable and performant code.
1. **More efficient collaboration**: With AI-assist code suggestions, developers should be able to share and reuse code snippets with their team members, leading to more efficient collaboration.
1. **Access to a broader range of knowledge**: The AI should be able to suggest code snippets and improvements across a wide range of programming languages and platforms, providing developers access to a broader range of knowledge.
1. **More effective use of time**: Developers should be able to spend more time on high-level tasks such as design and problem-solving, rather than on low-level tasks such as writing boilerplate code.
1. **Better adherence to best practices**: The AI should be able to suggest code snippets and improvements that adhere to best practices and industry standards, resulting in better quality software.
1. **More accurate and reliable code**: With the integration of AI, software development should be more accurate and reliable.

Overall, the integration of AI Assisted Code Suggestions should greatly improve the efficiency, quality, and speed of the software development process.


### Where we are Headed

1. **Develop** an AI model that can accurately suggest code snippets and improvements based on the context of the code being written. Though initially we will use popular open source models.
1. **Integrate** the AI model into popular IDEs such as Visual Studio and WebIDE.
1. **Continuously train and improve** the AI model using feedback from developers and communities.
1. **Add additional features** such as code refactoring suggestions and real-time error detection.
1. **Expand** support to more programming languages and platforms.
1. **Enhance** the user interface for an intuitive and seamless integration into the developer's workflow.
1. **Partner** with software development companies and open-source communities to gather feedback and improve the product.
1. **Create** a marketplace for developers to share their own code snippets and improve the overall AI's suggestions.
1. **Continuously** gather usage data and metrics to improve the overall product.
1. **Expand** to other development tools such as debugging and testing tools.


## Key Features

#### Increase developer productivity
1. **Support faster and accurate code completion** by analyzing the context in the file you are editing, as well as related files that are relevant to the task at hand
1. **Ability to pick from various suggestions including context** allowing for developers to direct recommendations towards a specific solution more easily. 
1. **Generate code suggestions from plain text comments** allowing suggestions to take direction and have greater context of developer intention.
1. **Generate suggestions for APIs and frameworks** allowing developers to fill in low value common boilerplate code.
1. **Ability to turn suggestions on/off as needed** extending greater control over the developer IDE experience.
2. **Ability to select a single line / lines of code to beautify** to help developers simplify their code and improve readability and align to coding conventions of projects. 
#### What is our Vision (Long-term Roadmap)

Revolutionizing the software development process through the integration of AI-assist code suggestions, resulting in increased productivity, reduced errors, and improved overall software quality.

#### What's Next & Why (Near-term Roadmap)

In the near-term we plan on expanding beyond our initial gated customers ( Closed Beta Release ) and intend to iterate toward an Open Beta.

* We are adding mnitoring and infra support in order to better scale and optimize code suggestions.
* Post Closed Beta we will evaluate and implement early customer feedback to enhance the model and backend.
* Developer will be able to access Code Suggestions from the (new) GitLab Web IDE.
* Multiple Suggestions & Developer UX improvements - We want code suggestions to feel natural and not get in the way, we'll be improving the UX of the suggestion experience to ensure we don't interrupt developers as they type and more smoothly make code suggestions. 
* Request Routing to Multiple Models for Improved Results - We are exploring merging models together via request routing to have multiple specialized models handling the request types they are best suited to handle.
* Exploration of code suggestions for GitLab-ci.yml files MVC - An additional use case that can help us pave the way for future training on customer code and provide GitLab unique value proposition.


#### What is Not Planned Right Now

Currently we are not developing a GitLab trained model, however that is something we may consider in the future. For now we will focus on open source code generation models.

#### Maturity Plan

This category is currently at minimal maturity. We will share further maturity plans as we receive feedback from our first beta experiences in the coming milestones.

### User success metrics

We plan to [measure the success of this category](https://gitlab.com/gitlab-org/gitlab/-/issues/375284) based on the the following metrics:

* **Developer cycle time** - Code Suggestions may make developers more efficient and hwe believe elp reduce the number of bugs, security vulnerabilities, and stylistic issues improving developer cycle time.
* **Time to merge** - With more efficient coding, merge requests will merge faster with less review time and back and forth between reviewers and authors.
* **Developer [SUS](/handbook/product/ux/performance-indicators/system-usability-scale/)** - Improved coding experience may directly improve the developer usability and satisfaction with the GitLab development experience.


## Competitive Landscape

As this category is new, we are still completing our evaluation of the competitive landscape.

## Analyst Landscape

As this category is new, we are actively engaging analysts on new reports and research, we'll share links to those as they are published
