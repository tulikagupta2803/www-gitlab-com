---
layout: sec_direction
title: "Category Direction - Security Policy Management"
description: "GitLab's Security Policy Management category provides unified policy and alert orchestration capabilities that span across the breadth of GitLab's security offerings."
canonical_path: "/direction/govern/security_policies/security_policy_management/"
---

- TOC
{:toc}

## Govern

| | |
| --- | --- |
| Stage | [Govern](/direction/govern/) |
| Maturity | [Minimal](/direction/maturity/) |
| Content Last Reviewed | `2023-02-22` |

### Introduction and how you can help

Thanks for visiting this category direction page on Security Policy Management in GitLab. This page belongs to the Security Policies group of the Govern stage and is maintained by Grant Hickman ([ghickman@gitlab.com](mailto:<ghickman@gitlab.com>)).

This direction page is a work in progress, and everyone can contribute. We welcome feedback, bug reports, feature requests, and community contributions.

 - Please comment and contribute in the linked [issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ASecurity%20Policy%20Management) and [epics](https://gitlab.com/groups/gitlab-org/-/epics/822) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
 - Please share feedback directly via email or on a video call. If you&apos;re a GitLab user and have direct knowledge of your need for security policies, we&apos;d especially love to hear from you.
 - Can&apos;t find an issue? Make a [feature proposal](https://gitlab.com/gitlab-org/gitlab/-/issues/new?issuable_template=Feature%20proposal%20-%20detailed) or a [bug report](https://gitlab.com/gitlab-org/gitlab/-/issues/new?&issuable_template=Bug). Please add the appropriate labels by adding this line to the bottom of your new issue `/label ~"devops::govern" ~"Category:Security Policy Management" ~"group::security policies"`.
<!--- https://gitlab.com/gitlab-org/gitlab/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=#) --->
- Consider signing up for [First Look](https://about.gitlab.com/community/gitlab-first-look/).

We believe [everyone can contribute](https://about.gitlab.com/company/mission/#contribute-to-gitlab-application) and so if you wish to contribute [here is how to get started](https://about.gitlab.com/community/contribute/).

## Overview
Security Policy Management is an overlay category that provides policy enforcement across all the scanners and technologies used by GitLab&apos;s Secure and Govern stages.  The goal is to provide a single, unified user experience that is consistent and intuitive.

### Target Audience
<!--
List the personas (https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas#user-personas) involved in this category.

Look for differences in user&apos;s goals or uses that would affect their use of the product. Separate users and customers into different types based on those differences that make a difference.
-->
1. [Cameron (Compliance Manager)](https://about.gitlab.com/handbook/product/personas/#cameron-compliance-manager)
1. [Devon (DevOps Engineer)](https://about.gitlab.com/handbook/product/personas/#devon-devops-engineer)
1. [Alex (Security Operations Engineer)](https://about.gitlab.com/handbook/product/personas/#alex-security-operations-engineer)
1. [Delaney (Development Team Lead)](https://about.gitlab.com/handbook/product/personas/#delaney-development-team-lead)

### Key goals and guiding principles

1. **Scalability** - Allow organizations with large numbers of projects to centrally manage and enforce when scans are run
1. **Ease of use** - Lower the knowledge requirement to use GitLab&apos;s scanners
1. **Appropriate permissions** - Limit who can make policy changes; support auditing and approvals
1. **Unified experience** - Provide a consistent way to manage policies regardless of scanner or technology type
1. **Flexibility** - Allow users to work in either a GUI or code based on their preferences

## Key Features

### Security Policies

 Security policies allow users to use a single, simple UI to define rules and actions that are then enforced.  Two types of policies are currently supported: scan execution policies and scan result policies. Security policies themselves are fully audited and can be configured to go through a two-step approval process before any changes are made.  All policies are supported at the group, sub-group, and project levels.

 **Scan Execution Policies** allow users to require vulnerability scans to be run, either on a specified schedule or as part of a pipeline job. Currently we support requiring the execution of [SAST, Secret Detection, Container Scanning, Dependency Scanning, and DAST scans](https://docs.gitlab.com/ee/user/application_security/policies/scan-execution-policies.html).  We do not plan on adding support for License Compliance as its functionality is [planned to be merged into Dependency Scanning](https://gitlab.com/groups/gitlab-org/-/epics/8072).  We do intend to add support for Fuzzing; however, this is not on our near-term roadmap.

 **Scan Result Policies** allow users to enforce approval on a merge request when policy conditions are violated. Currently criteria related to both security and license scanners are supported.  For example, users can require approval on merge requests that introduce newly-detected, critical vulnerabilities into their application.

[Learn more](https://docs.gitlab.com/ee/user/application_security/policies/)

#### Security Approvals

<p align="center">
    <img src="/images/direction/govern/security-approvals.png" style="border: 1px solid gray" alt="Security Approvals">
</p>

Security approvals allow users to select the conditions that must be met to trigger the security approval rule, including which branches, scanners, vulnerability count, and vulnerability severity levels must be present in the MR.  If all conditions are met, then the merge request is blocked unless an eligible user approves the MR. This extra layer of oversight can serve as an enforcement mechanism as part of a strong security compliance program.

Security approvals are a type of Scan Result Security Policy and can be configured in the **Security & Compliance > Policies** page.

[Learn more](https://docs.gitlab.com/ee/user/application_security/policies/scan-result-policies.html)

#### License Approvals

<p align="center">
    <img src="/images/direction/govern/license-approvals.png" style="border: 1px solid gray" alt="License Approvals">
</p>

License approvals allow users to select the conditions that must be met to trigger the license approval rule, including which licenses are expressly allowed or prohibited from being present in the MR.  If all conditions are met, then the merge request is blocked unless an eligible user approves the MR. This extra layer of oversight can serve as an enforcement mechanism as part of a strong legal compliance program.

License approvals are a type of Scan Result Security Policy and can be configured in the **Security & Compliance > Policies** page.

[Learn more](https://docs.gitlab.com/ee/user/compliance/license_approval_policies.html)

## Vision and Roadmap

### What is our Vision (Long-term Roadmap)

In the long-run, we intend to add support for additional policy types, including Vulnerability Management, Compliance, Insider Threat, and Pipeline Execution policies.  Furthermore, we intend to provide visibility into the impact (blast radius) a policy will have before it is deployed, add support for complex orchestration policies, and auto-suggest policies based on your unique environment and codebase.

Beyond policies, we also intend to add support for an Alert workflow that will allow users to be notified of events that require manual, human review to determine the next steps to take.  This workflow will eventually support auto-suggested responses and recommended changes to policies to reduce false positives and automate responses whenever possible.  It will also provide an interface for security and compliance teams to review and respond to policy violations that have occurred.

The matrixes below describe the scope of the work that is planned in the long-run for the Security Policy Management category as well as our progress toward the end goal.  Our long-term vision is to add support for all of the boxes in the table.

![Security Policy Management Roadmap](/images/direction/govern/security-policy-roadmap.png)

### What's Next & Why (Near-term Roadmap)

In GitLab 15.9 we expanded our Scan Result Policy support to also include the ability to trigger approval for license-based criteria by allowing users to create [License Approval Policies](https://docs.gitlab.com/ee/user/compliance/license_approval_policies.html). These policies are designed to be a replacement for the current `License-Check` functionality.

Next, in GitLab 15.10, we plan on adding support for [Role Based Approvers](https://gitlab.com/groups/gitlab-org/-/epics/8018) rather than only allowing either users or groups to approve a policy violation.  Further down the roadmap, we also plan to [add more granular criteria](https://gitlab.com/groups/gitlab-org/-/epics/6826) for our Scan Result policies, including allowing users to choose whether or not to require approval for vulnerabilities that are marked as false positives or that do not have a known fix available.

Once we finish improving our current set of functionality, we plan on consolidating the capabilities of Compliance Framework Pipelines with Security Policies to provide for a single, unified experience. More details on these roadmap plans can be viewed [in this video](https://youtu.be/grg_M1MtiYw?t=10) or in the [design mocks in this issue](https://gitlab.com/gitlab-org/gitlab/-/issues/379123).

Additionally, we plan to introduce a [new type of policy](https://gitlab.com/groups/gitlab-org/-/epics/9704) to allow users to enforce approval requirements for all merge requests independent of security or license scan results.

See a full list of our [near-term priorities](https://about.gitlab.com/direction/govern/security_policies/#priorities) on our group direction page.

### What is Not Planned Right Now

We do not currently plan to be a full-featured SOAR solution capable of aggregating, correlating, and enriching events from multiple security vendors.  We intend to remain focused on providing security management and security orchestration for the security tools that are part of the GitLab product only.

## Maturity Plan

This category is currently at Minimal maturity.  A [plan has been created](https://gitlab.com/groups/gitlab-org/-/epics/4595) for the category to progress to Viable maturity.

### User success metrics
<!--
- What specific user behaviors are indicate that users are trying these features, and solving their problems?
- How will users discover these features?
-->
We plan to [measure the success of this category](https://gitlab.com/gitlab-org/gitlab/-/issues/375284) based on the the following metrics:

1. The total number of projects with an assigned security policy project.
1. The total number of groups with an assigned security policy project.
1. The total number of projects with at least one scan result policy.
1. The total number of open merge requests with at least one applicable scan result policy.
1. The total number of users who have created merge requests in Projects that have an assigned security policy project.

### Competitive Landscape

As this category is new, we are still completing our evaluation of the competitive landscape.

### Analyst Landscape

As this category is new, we have not yet engaged analysts on this topic.

## Priorities

This section was recently moved to the [group level Security Policies direction page](https://about.gitlab.com/direction/govern/security_policies/#priorities).

*This page may contain information related to upcoming products, features and functionality.

It is important to note that the information presented is for informational purposes only, so please do not rely on the information for purchasing or planning purposes.

Just like with all projects, the items mentioned on the page are subject to change or delay, and the development, release, and timing of any products, features, or functionality remain at the sole discretion of GitLab Inc.*
